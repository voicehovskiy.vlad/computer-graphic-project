const WIDTH = 450, HEIGHT = 300, ITERATIONS = 200, ZOOM_INDEX = 0.2, MOVE_INDEX = 50, COLOR_INDEX = 1;
let zoom = 1, moveX = 0, moveY = 0, colorChange = 1;



let canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d');

function ComplexNumber(re, im) {
    this.re = re;
    this.im = im;
}

ComplexNumber.prototype.add = function(other){
    return new ComplexNumber(this.re + other.re, this.im + other.im);
}

ComplexNumber.prototype.mul = function(other){
    return new ComplexNumber(this.re * other.re - this.im * other.im, this.re * other.im + this.im * other.re);
}

ComplexNumber.prototype.abs = function(){
    return Math.sqrt(this.re * this.re + this.im * this.im);
}

ComplexNumber.prototype.sin = function(other){
    let xr = this.re,
        xi = this.im,
        wr = other.re,
        wi = other.im,
        ta = Math.cos(xr),
        tb = Math.sin(xr),
        tc = Math.exp(xi),
        td = Math.exp(-xi),
        te = 0.5 * tb * (tc + td),
        tf = 0.5 * ta * (tc - td),
        zr = wr * te - wi * tf,
        zi = wi * te + wr * tf;

    return new ComplexNumber(zr, zi);
}


function belongs(re, im, iterations){

    let z = new ComplexNumber(Math.PI/2,Math.PI/2),
        c = new ComplexNumber(re, im),
        i = 0;
    while(z.abs() < 40  && i < iterations){
        z = c.mul(z.sin(c));
        i++;
    }
    return i;
}

function pixel(x, y, color){
    ctx.fillStyle = color;
    ctx.fillRect(x*zoom + moveX, y*zoom + moveY, 8*zoom, 8*zoom);
}

function draw(width, height, maxIterations){
    let minRe = -3, maxRe = 3, minIm = -3, maxIm = 3, re = minRe,
        reStep = (maxRe - minRe) / width, imStep = (maxIm - minIm) / height;

    while(re < maxRe){
        let im = minIm;
        while(im < maxIm){
            let result = belongs(re, im, maxIterations);
            let x = (re - minRe) / reStep, y = (im - minIm) / imStep;

            if(result === maxIterations){
                pixel(x, y, 'black');
            }else{
                let h = 30 + Math.round(120 * result * 1.0 / maxIterations),
                    color = 'hsl(' + h * colorChange + ', 100%, 50%)';
                pixel(x, y, color);
            }
            im += imStep/zoom;
        }
        re += reStep/zoom;
    }
}

draw(WIDTH, HEIGHT, ITERATIONS);

document.getElementById('btn-download').addEventListener("click", function(e) {
    let canvas = document.querySelector('canvas');

    let dataURL = canvas.toDataURL("image/jpeg", 1.0);

    downloadImage(dataURL, 'fractal.jpeg');
});

function downloadImage(data, filename = 'untitled.jpeg') {
    let a = document.createElement('a');
    a.href = data;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
}

